package ImplTest;

import Impl.TodoBusinessImpl;
import InterfaceTest.TodoService;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TodoBussinessImplMockTest {

    @Test
    public void testRetrieveTodosRelatedToSpring_usingMock(){
        //Mocking a dependency
        TodoService todoServiceMock = mock(TodoService.class);

        List<String> todos = Arrays.asList("Learn Spring MVC","Learn Spring", "Learn to Dance on the");

        when(todoServiceMock.retrieveTodos("Dummy")).thenReturn(todos);

        //?
        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        List<String> filteredTodos = todoBusinessImpl.retrieveTodosRelatedToSpring("Dummy");
        assertEquals(2, filteredTodos.size());
        //assertEquals("Learn Spring MVC", filteredTodos.size( ));
    }
}